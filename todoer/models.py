from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

db = SQLAlchemy()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(80), unique=True)
    email = db.Column(db.String(120), unique=True)
    display_name = db.Column(db.String(120))
    created_on = db.Column(db.DateTime, default=datetime.now())
    tasks = db.relationship('Task', backref=db.backref('user', lazy='joined'), lazy="dynamic")

    def __repr__(self):
        return '<User %r>' % self.username

    @property
    def serialize(self):
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'display_name': self.display_name,
            'created on': dump_datetime(self.created_on)
        }


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    description = db.Column(db.String(240))
    completed = db.Column(db.Boolean, default=False)
    created_on = db.Column(db.DateTime, default=datetime.now())
    completed_on = db.Column(db.DateTime, default=None)
    due_on = db.Column(db.DateTime, default=None)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Task %r>' % self.description

    @property
    def serialize(self):
        return {
            'id': self.id,
            'description': self.description,
            'completed': self.completed,
            'created on': dump_datetime(self.created_on),
            'completed on': dump_datetime(self.completed_on),
            'due on': dump_datetime(self.due_on)
        }


def dump_datetime(value):
    """Deserialize datetime object into string form for JSON processing."""
    if value is None:
        return None
    return [value.strftime("%Y-%m-%d"), value.strftime("%H:%M:%S")]
