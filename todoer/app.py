"""
todoer.app
~~~~~~~~~~~~~~~~~~~~~~

Main flask application, uwsgi module to start
"""
from flask import Flask, jsonify, request, make_response, abort, render_template, redirect, url_for
from datetime import datetime
from todoer.models import db, Task, User


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://nmillward:12qw12qw@localhost/todoer'

db.init_app(app)


@app.route('/api')
def home():
    return jsonify({

    })


# Task Input Form
@app.route('/api/task')
def input_task():
    return render_template(
        'add_task.html'
    )


# User Input Form
@app.route('/api/user')
def input_user():
    return render_template(
        'add_user.html'
    )


# Show All Task
@app.route('/api/tasks', methods=['GET'])
def get_tasks():
    tasks = Task.query.all()
    return jsonify(
        [i.serialize for i in tasks]
    )


# Create Task
@app.route('/api/tasks', methods=['POST'])
def create_task():
    # if not request.json:
    #     abort(400)
    task = Task(
        description=request.form['description'],
        created_on=datetime.now()
    )
    db.session.add(task)
    db.session.commit()
    return redirect(
        url_for('input_task')
    )


# Get A Task
@app.route('/api/tasks/<int:task_id>', methods=['GET'])
def get_task(task_id):
    task = get_result_or_abort(Task, task_id)
    return jsonify(
        task.serialize
    )


# Modify Task
@app.route('/api/tasks/<int:task_id>', methods=['PUT'])
def modify_task(task_id):
    if not request.json:
        abort(400)
    task = get_result_or_abort(Task, task_id)
    task.description = request.json.get('description', task.description)
    task.completed = request.json.get('completed', task.completed)
    task.due_on = request.json.get('due_on', task.due_on)
    db.session.commit()
    return jsonify(
        task.serialize
    )


# Delete Task
@app.route('/api/tasks/<int:task_id>', methods=['DELETE'])
def delete_task(task_id):
    task_to_delete = get_result_or_abort(Task, task_id)
    db.session.delete(task_to_delete)
    db.session.commit()
    return jsonify({
        'result': True
    })


# Display All Users
@app.route('/api/users', methods=['GET'])
def get_all_user():
    users = User.query.all()
    return jsonify(
        [i.serialize for i in users]
    )


# Sign Up or Add User
@app.route('/api/users', methods=['POST'])
def add_user():
    # if not request.json:
    #     abort(400)
    user = User(
        username=request.form['username'],
        email=request.form['email'],
        display_name=request.form['display_name'],
        created_on=datetime.now()
    )
    db.session.add(user)
    db.session.commit()
    return redirect(
        url_for('input_user')
    )


# Get User Info
@app.route('/api/users/<int:user_id>', methods=['GET'])
def get_user_info(user_id):
    user = get_result_or_abort(User, user_id)
    return jsonify(
        user.serialize
    )


# Show All Tasks Per User
@app.route('/api/users/<int:user_id>/tasks', methods=['GET'])
def get_user_tasks(user_id):
    tasks = Task.query.filter_by(user_id=user_id).all()
    return jsonify(
        [i.serialize for i in tasks]
    )


# Allow users to add notes to their tasks


# Return result or Abort helper method
def get_result_or_abort(model, object_id, code=404):
    result = model.query.get(object_id)
    return result or abort(code)


# 404 response
@app.errorhandler(404)
def not_found(error):
    return make_response(
        jsonify({'error': 'Not Found'}), 404
    )
